package com.telenav.internship;


import com.telenav.internship.services.BorderLengthComputer;
import com.telenav.internship.services.NodesOnBorderExtractor;
import com.telenav.internship.services.PlacesNamesLinker;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;


public class BorderLengthsForCitiesApp {

    private static final String nodesPath = "src/main/resources/romania-latest.osm.pbf.node.parquet";
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    public static void main(final String[] args) {
        final SparkSession sparkSession = SparkSession.builder()
                .master("local[*]")
                .appName("RomaniaMapAnalyzer")
                .config("spark.sql.parquet.binaryAsString", "true")
                .getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");

        //TODO too many spaces, can be grouped like below:
        final Dataset<Row> nodes = sparkSession.read().parquet(nodesPath);
        final Dataset<Row> ways = sparkSession.read().parquet(waysPath);

        final Dataset<Row> simplifiedNodes =
                nodes.select(col("id").as("simplifiedId"), col("longitude"), col("latitude"));

        final BorderLengthComputer borderLengthComputer = new BorderLengthComputer(sparkSession);
        final NodesOnBorderExtractor nodesOnBorderExtractor = new NodesOnBorderExtractor(sparkSession);
        final Dataset<Row> pairwiseBordersAndNodes = nodesOnBorderExtractor.extractBorders(ways);
        final Dataset<Row> placesWithBorderLength =
                borderLengthComputer.computeLengths(pairwiseBordersAndNodes, simplifiedNodes)
                        .withColumnRenamed("id", "borderId");

        //TODO do not forget to remove debug messages, in this case the intermediate show()
        placesWithBorderLength.show(false);

        final PlacesNamesLinker placesNamesLinker = new PlacesNamesLinker(ways);
        placesNamesLinker.provideNames(placesWithBorderLength)
                .orderBy(col("borderLength").desc()).drop(col("id"))
                .show(5000, false);
    }
}