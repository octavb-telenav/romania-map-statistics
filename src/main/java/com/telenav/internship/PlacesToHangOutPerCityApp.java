package com.telenav.internship;

import com.telenav.internship.services.*;
import com.telenav.internship.udf.IsPlaceToHangOutUDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;


public class PlacesToHangOutPerCityApp {

    private static final String nodesPath = "src/main/resources/romania-latest.osm.pbf.node.parquet";
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    public static void main(final String[] args) {
        //TODO add the metho d calls one below the other
        // .master("local[*]")
        // .appName("RomaniaMapAnalyzer")
        // .config("spark.sql.parquet.binaryAsString", "true") and so on

        final SparkSession sparkSession = SparkSession.builder().master("local[*]").appName("RomaniaMapAnalyzer")
                .config("spark.sql.parquet.binaryAsString", "true").getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");
        sparkSession.udf().register("isPlaceToHangOut", new IsPlaceToHangOutUDF(), DataTypes.BooleanType);
        final Dataset<Row> nodes = sparkSession.read().parquet(nodesPath);
        final Dataset<Row> ways = sparkSession.read().parquet(waysPath);

        //TODO to many blank lines, organize them like I did below for you.
        nodes.filter(callUDF("isPlaceToHangOut",col("tags"))).show(false);

        final NodesOnBorderExtractor nodesOnBorderExtractor = new NodesOnBorderExtractor(sparkSession);
        final BorderBoxExtractor borderBoxExtractor = new BorderBoxExtractor(sparkSession, nodesOnBorderExtractor);
        final Dataset<Row> borderExtremities = borderBoxExtractor.extractBorderBox(ways, nodes).cache();

        final PlaceFinder placeFinder = new PlaceFinder(sparkSession);
        final Dataset<Row> placesToHangOut = placeFinder.extractPlacesToHangOut(nodes).cache();

        final PlacesInsideBordersExtractor placesInsideBordersExtractor = new PlacesInsideBordersExtractor();
        final Dataset<Row> citiesAndNrOfPlaces = placesInsideBordersExtractor
                .getNrOfPlacesInsideBorders(borderExtremities, placesToHangOut);

        final PlacesNamesLinker placesNamesLinker = new PlacesNamesLinker(ways);
        placesNamesLinker.provideNames(citiesAndNrOfPlaces)
                .orderBy(col("count").desc())
                .drop(col("id"))
                .show(5000,false);
    }
}