package com.telenav.internship.services;

import com.telenav.internship.udf.GetMaxOfDoublesUDF;
import com.telenav.internship.udf.GetMinOfDoublesUDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.collect_list;


public class BorderBoxExtractor {

    private final SparkSession sparkSession;

    private final NodesOnBorderExtractor nodesOnBorderExtractor;

    public BorderBoxExtractor(final SparkSession sparkSession, final NodesOnBorderExtractor nodesOnBorderExtractor) {
        this.sparkSession = sparkSession;
        this.nodesOnBorderExtractor = nodesOnBorderExtractor;
    }

    public Dataset<Row> extractBorderBox(final Dataset<Row> ways, final Dataset<Row> nodes){
        registerUDFs();

        final Dataset<Row> pairwiseBordersAndNodes =
                nodesOnBorderExtractor.extractBorders(ways).withColumnRenamed("id", "id1");

        final Dataset<Row> nodesJoinedWithWays =
                pairwiseBordersAndNodes
                        .join(nodes, nodes.col("id").equalTo(pairwiseBordersAndNodes.col("nodeId")));


        final Dataset<Row> minMaxLatitudesForBorders = getLatitudeExtremities(nodesJoinedWithWays);

        final Dataset<Row> minMaxLongitudeForBorders = getLongitudeExtremities(nodesJoinedWithWays);

        return minMaxLatitudesForBorders
                .join(minMaxLongitudeForBorders, minMaxLatitudesForBorders
                        .col("borderId").equalTo(minMaxLongitudeForBorders.col("borderId2")))
                .drop("borderId2");
    }

    private void registerUDFs(){
        sparkSession.udf().register("minOfDoubles", new GetMinOfDoublesUDF(), DataTypes.DoubleType);
        sparkSession.udf().register("maxOfDoubles", new GetMaxOfDoublesUDF(), DataTypes.DoubleType);
    }

    private Dataset<Row> getLatitudeExtremities(final Dataset<Row> borders){
        final Dataset<Row> bordersWithNodeLatitudes =
                borders.groupBy(col("id1"))
                        .agg(collect_list(col("latitude")))
                        .toDF("borderId", "nodeLatitudes");

        return bordersWithNodeLatitudes
                        .withColumn("minLatitude", callUDF("minOfDoubles", col("nodeLatitudes")))
                        .withColumn("maxLatitude", callUDF("maxOfDoubles", col("nodeLatitudes")))
                        .drop(col("nodeLatitudes"));

    }

    private Dataset<Row> getLongitudeExtremities(final Dataset<Row> borders){
        final Dataset<Row> bordersWithNodeLongitudes =
                borders.groupBy(col("id1"))
                        .agg(collect_list(col("longitude")))
                        .toDF("borderId2", "nodesLongitudes");

        return bordersWithNodeLongitudes
                        .withColumn("minLongitude", callUDF("minOfDoubles", col("nodesLongitudes")))
                        .withColumn("maxLongitude", callUDF("maxOfDoubles", col("nodesLongitudes")))
                        .drop("nodesLongitudes");
    }
}