package com.telenav.internship.services;

import com.telenav.internship.udf.GetBorderLengthUDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;


public class BorderLengthComputer {

    private final SparkSession sparkSession;


    public BorderLengthComputer(final SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Row> computeLengths(final Dataset<Row> waysWithNodeInfo, final Dataset<Row> simplifiedNodes) {
        registerUDFs();

        final Dataset<Row> nodeIdsWithPairsOfLatLongForWay =
                pairNodeCoordinatesForWay(waysWithNodeInfo, simplifiedNodes);

        return nodeIdsWithPairsOfLatLongForWay.groupBy(col("id"))
                        .agg(collect_list(col("latLong")))
                        .toDF("id", "latLongs")
                        .withColumn("borderLength", callUDF("getBorderLength", col("latLongs")))
                        .drop(col("latLongs"));
    }

    private Dataset<Row> pairNodeCoordinatesForWay(final Dataset<Row> wayIdsIndexesNodeIds, final Dataset<Row> nodes){
        return wayIdsIndexesNodeIds
                .join(nodes, wayIdsIndexesNodeIds.col("nodeId").equalTo(nodes.col("simplifiedId")))
                .withColumn("latLong", struct(col("index"), col("longitude"), col("latitude")))
                .drop("latitude", "longitude").orderBy(col("id").asc(), col("index").asc()).drop("simplifiedId");

    }

    private void registerUDFs() {
        sparkSession.udf().register("getBorderLength", new GetBorderLengthUDF(), DataTypes.DoubleType);
    }
}
