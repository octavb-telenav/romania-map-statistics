package com.telenav.internship.services;

import com.telenav.internship.udf.HasAdminLevel6UDF;
import com.telenav.internship.udf.IsCityOrTownUDF;
import com.telenav.internship.udf.IsNotCoastlineUDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.*;


public class NodesOnBorderExtractor {

    private final SparkSession sparkSession;

    public NodesOnBorderExtractor(final SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Row> extractBorders(final Dataset<Row> ways) {
        registerUDFs();
        return ways.filter(callUDF("isNotCoastline", col("tags"))).filter(callUDF("hasAdminLevel6", col("tags")))
                .filter(callUDF("isCityOrTown", col("tags"))).select(col("id"), explode(col("nodes")).as("node"))
                .select(col("id"), col("node.nodeId").as("nodeId"), col("node.index"));
    }

    private void registerUDFs() {
        sparkSession.udf().register("hasAdminLevel6", new HasAdminLevel6UDF(), DataTypes.BooleanType);
        sparkSession.udf().register("isNotCoastline", new IsNotCoastlineUDF(), DataTypes.BooleanType);
        sparkSession.udf().register(("isCityOrTown"), new IsCityOrTownUDF(), DataTypes.BooleanType);
    }
}