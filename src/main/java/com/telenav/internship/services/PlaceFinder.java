package com.telenav.internship.services;

import com.telenav.internship.udf.IsPlaceToHangOutUDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;

import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;


public class PlaceFinder {

    private final SparkSession sparkSession;

    public PlaceFinder(final SparkSession sparkSession) {
        this.sparkSession = sparkSession;
    }

    public Dataset<Row> extractPlacesToHangOut(final Dataset<Row> nodes){
        registerUDFs();
        return nodes.filter(callUDF("isPlaceToHangOut", col("tags")));
    }

    private void registerUDFs(){
        sparkSession.udf().register("isPlaceToHangOut", new IsPlaceToHangOutUDF(), DataTypes.BooleanType);
    }
}
