package com.telenav.internship.services;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static org.apache.spark.sql.functions.col;


public class PlacesInsideBordersExtractor {
    //TODO
    /*
        Write join like:
        return  borderExtremities.join(places, borderExtremities.col("minLatitude").$less$eq(places.col("latitude"))
                        .and(borderExtremities.col("maxLatitude").$greater$eq(places.col("latitude")))
                        .and(borderExtremities.col("maxLongitude").$greater$eq(places.col("longitude")))
                        .and(borderExtremities.col("minLongitude").$less$eq(places.col("longitude"))))
                .groupBy("borderId")
                .count()
                .orderBy(col("count").desc());

        Easier to read.

        Take it one step further and create a method, see joinCodition added by me. Call it like:
         borderExtremities
                    .join(places, joinCodition(borderExtremities, places))
                    .groupBy("borderId").count()
                    .orderBy(col("count").desc());
     */

    public Dataset<Row> getNrOfPlacesInsideBorders(final Dataset<Row> borderExtremities, final Dataset<Row> places){
        return  borderExtremities
                    .join(places, joinCodition(borderExtremities, places))
                    .groupBy("borderId").count()
                    .orderBy(col("count").desc());

    }

    private Column joinCodition(Dataset<Row> borderExtremities, Dataset<Row> places) {
        return borderExtremities.col("minLatitude").$less$eq(places.col("latitude"))
                .and(borderExtremities.col("maxLatitude").$greater$eq(places.col("latitude")))
                .and(borderExtremities.col("maxLongitude").$greater$eq(places.col("longitude")))
                .and(borderExtremities.col("minLongitude").$less$eq(places.col("longitude")));
    }
}
