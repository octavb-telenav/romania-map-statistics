package com.telenav.internship.services;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static org.apache.spark.sql.functions.*;


public class PlacesNamesLinker {

    private final Dataset<Row> ways;
    public PlacesNamesLinker(final Dataset<Row> ways) {
        this.ways = ways;
    }

    public Dataset<Row> provideNames(final Dataset<Row> places){
        final Dataset<Row> placesWithNames = ways.select(col("id"), explode(col("tags")).as("name"))
                .filter(col("name.key").equalTo("name"));
        return places.join(placesWithNames, placesWithNames.col("id").equalTo(places.col("borderId")))
                .drop(col("id"));
    }
}
