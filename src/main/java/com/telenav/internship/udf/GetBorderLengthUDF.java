package com.telenav.internship.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.List;


public class GetBorderLengthUDF implements UDF1<Seq<Row>, Double> {

    @Override
    public Double call(final Seq<Row> rowSeq) {
        final List<Row> list = JavaConversions.seqAsJavaList(rowSeq);
        Row a = list.get(0);
        Double sum = 0.0;
        for (int i = 1; i < list.size(); i++) {
            sum += distance(a.getAs("latitude"), list.get(i).getAs("latitude"), a.getAs("longitude"),
                    list.get(i).getAs("longitude"));
            a = list.get(i);
        }
        return sum;
    }

    private static double distance(double lat1, double lat2, double lon1, double lon2) {
        lat1 = Math.toRadians(lat1);
        lon1 = Math.toRadians(lon1);
        lat2 = Math.toRadians(lat2);
        lon2 = Math.toRadians(lon2);
        final double earthRadius = 6371.01;
        return earthRadius * Math
                .acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));
    }
}