package com.telenav.internship.udf;

import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;


public class GetMaxOfDoublesUDF implements UDF1<Seq<Double>, Double> {

    @Override
    public Double call(final Seq<Double> doubleSeq) {
        final List<Double> numbers = JavaConversions.seqAsJavaList(doubleSeq);
        return numbers.stream()
                .max(Comparator.naturalOrder()).get();
        //TODO get() call without checking ifPresent().No need to put max on seperate line.
        /*
              Optional<Double> max = numbers.stream().max(Comparator.naturalOrder());
              return max.orElse(0.0);
         */
    }
}