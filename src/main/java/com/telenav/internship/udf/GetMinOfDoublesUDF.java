package com.telenav.internship.udf;

import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.Comparator;
import java.util.List;


public class GetMinOfDoublesUDF implements UDF1<Seq<Double>, Double> {

    @Override
    public Double call(final Seq<Double> doubleSeq) {
        final List<Double> numbers = JavaConversions.seqAsJavaList(doubleSeq);
        return numbers.stream()
                .min(Comparator.naturalOrder()).get();
        //TODO same here get() without isPresent, also unecessary new line
    }
}