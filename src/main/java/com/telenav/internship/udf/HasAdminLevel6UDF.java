package com.telenav.internship.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.List;


public class HasAdminLevel6UDF implements UDF1<Seq<Row>, Boolean> {

    @Override
    public Boolean call(final Seq<Row> tags) {
        final List<Row> rows = JavaConversions.seqAsJavaList(tags);
        return rows.stream()
                .anyMatch(row -> row.getAs("key").equals("admin_level") && row.getAs("value").equals("6"));
        //TODO no need for new line
    }
}