package com.telenav.internship.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.List;


public class IsCityOrTownUDF implements UDF1<Seq<Row>, Boolean> {


    @Override
    public Boolean call(final Seq<Row> tags) {
        final List<Row> rows = JavaConversions.seqAsJavaList(tags);
        return rows.stream()
                .anyMatch(row -> row.getAs("key").equals("place") &&
                        (row.getAs("value").equals("town") || row.getAs("value").equals("city")));
        /*
            TODO how I would format it
            return rows.stream().anyMatch(row -> row.getAs("key").equals("place")
                && (row.getAs("value").equals("town") || row.getAs("value").equals("city")));
         */
    }
}