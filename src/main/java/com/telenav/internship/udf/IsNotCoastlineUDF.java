package com.telenav.internship.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.List;


public class IsNotCoastlineUDF implements UDF1<Seq<Row>, Boolean> {

    @Override
    public Boolean call(final Seq<Row> tags) {
        final List<Row> rows = JavaConversions.seqAsJavaList(tags);
        return rows.stream()
                .noneMatch(x -> x.getAs("key").equals("natural") && x.getAs("value").equals("coastline"));
        //TODO no need for new line at noneMatch, use row instead of x
    }
}