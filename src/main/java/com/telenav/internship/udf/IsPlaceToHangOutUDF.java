package com.telenav.internship.udf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.Seq;

import java.util.List;


public class IsPlaceToHangOutUDF implements UDF1<Seq<Row>, Boolean> {

    @Override
    public Boolean call(final Seq<Row> tags) {
        final List<Row> rows = JavaConversions.seqAsJavaList(tags);
        return rows.stream().anyMatch(
                row -> row.getAs("key").equals("amenity") && (row.getAs("value").equals("pub") ||
                        row.getAs("value").equals("restaurant") || row.getAs("value").equals("bar")));
    }


    //TODO make a method isBar as below
    //call it like:  return rows.stream().anyMatch(this::isBar);
    private Boolean isBar(Row row) {
        return  row.getAs("key").equals("amenity") && (row.getAs("value").equals("pub")
                    || row.getAs("value").equals("restaurant") || row.getAs("value").equals("bar"));
    }
}