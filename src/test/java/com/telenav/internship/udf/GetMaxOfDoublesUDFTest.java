package com.telenav.internship.udf;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.junit.BeforeClass;
import org.junit.Test;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class GetMaxOfDoublesUDFTest {

    @BeforeClass
    public static void init(){

    }

    @Test
    public void returnMaxValueCorrectly(){
        final List<Double> doubleList = Arrays.asList(1.0,2.0,3.0);
        final Seq<Double> doubleSeq = JavaConverters.asScalaIteratorConverter(doubleList.iterator()).asScala().toSeq();
        assertEquals(3.0, new GetMaxOfDoublesUDF().call(doubleSeq), 0.0);
    }

}