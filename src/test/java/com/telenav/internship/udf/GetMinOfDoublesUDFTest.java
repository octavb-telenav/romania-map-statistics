package com.telenav.internship.udf;

import com.twitter.chill.java.ArraysAsListSerializer;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.junit.BeforeClass;
import org.junit.Test;
import scala.collection.JavaConversions;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class GetMinOfDoublesUDFTest {

    private static SparkSession sparkSession;
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    @BeforeClass
    public static void init(){

        sparkSession = SparkSession.builder().master("local[*]").appName("Book Classification Application")
                .config("spark.sql.parquet.binaryAsString", "true").getOrCreate();

        sparkSession.sparkContext().setLogLevel("ERROR");
        sparkSession.udf().register("minOfDoubles", new GetMinOfDoublesUDF(), DataTypes.DoubleType);

    }

    @Test
    public void returnMinValueCorrectly(){
        List<Double> doubleList = Arrays.asList(1.0,2.0,3.0);
        Seq<Double> doubleSeq = JavaConverters.asScalaIteratorConverter(doubleList.iterator()).asScala().toSeq();
        assertTrue(new GetMinOfDoublesUDF().call(doubleSeq) == 1.0);
    }
}