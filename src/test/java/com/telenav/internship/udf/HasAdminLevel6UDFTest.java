package com.telenav.internship.udf;

import com.telenav.internship.udf.HasAdminLevel6UDF;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.callUDF;


public class HasAdminLevel6UDFTest {

    private static SparkSession sparkSession;
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    @BeforeClass
    public static void init(){

        sparkSession = SparkSession.builder().master("local[*]").appName("Book Classification Application")
                .config("spark.sql.parquet.binaryAsString", "true").getOrCreate();

        sparkSession.sparkContext().setLogLevel("ERROR");
        sparkSession.udf().register("hasAdminLevel6", new HasAdminLevel6UDF(), DataTypes.BooleanType);

    }

    @Test
    public void identifiesAdminLevel6CorrectlyWhenExists() {
        Dataset<Row> ways = sparkSession.read().parquet(waysPath);

        Dataset<Row> adminLevel6 = ways.filter(callUDF("hasAdminLevel6", col("tags")));
        assertTrue(adminLevel6.count() != 0);

    }

    @Test
    public void identifiesAdminLevel6CorrectlyWhenNotExists() {
        Dataset<Row> ways = sparkSession.read().parquet(waysPath);

        Dataset<Row> adminLevel5 = ways.select(col("id"), explode(col("tags")).as("adminLevel"))
                .filter(col("adminLevel.key").equalTo("admin_level").and(not(
                        col("adminLevel.value").equalTo("6"))));
        Dataset<Row> adminLevel6 = adminLevel5.filter(callUDF("hasAdminLevel6", col("tags")));
        assertTrue(adminLevel6.count() == 0);

    }
}