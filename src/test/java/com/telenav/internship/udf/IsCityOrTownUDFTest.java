package com.telenav.internship.udf;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.apache.spark.sql.functions.*;


public class IsCityOrTownUDFTest {

    private static SparkSession sparkSession;
    private static final String nodesPath = "src/main/resources/romania-latest.osm.pbf.node.parquet";
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    @BeforeClass
    public static void init() {
        sparkSession =
                SparkSession.builder().master("local[*]").appName("Book Classification Application")
                .config("spark.sql.parquet.binaryAsString", "true").getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");
        sparkSession.udf().register(("isCityOrTown"), new IsCityOrTownUDF(), DataTypes.BooleanType);
    }

    @Test
    public void verifyIfFindingWorksWhenCityOrTownExists() {
        Dataset<Row> ways = sparkSession.read().parquet(waysPath);
        Dataset<Row> minimizedWays = ways.select(ways.col("tags"), ways.col("id"));
        Dataset<Row> cityOrTown = minimizedWays.filter(callUDF("isCityOrTown", col("tags")));
        cityOrTown.show(false);

        long count = cityOrTown.count();
        assertTrue(count > 0);

    }

    @Ignore
    @Test
    public void verifyIfFindingWorksWhenCityOrTownDoesNotExist() {
        Dataset<Row> nodes = sparkSession.read().parquet(nodesPath);

        Dataset<Row> notCityOrTown = nodes.filter(not(callUDF("isCityOrTown", col("tags"))));

        notCityOrTown.show(false);

        Dataset<Row> cityOrTown =
                notCityOrTown.filter(callUDF("isCityOrTown", col("tags")));


        assertTrue(cityOrTown.count() == 0);
    }

    @AfterClass
    public static void tearDown() {
        sparkSession.close();
    }
}