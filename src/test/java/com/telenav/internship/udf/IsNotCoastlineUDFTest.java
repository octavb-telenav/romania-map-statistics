package com.telenav.internship.udf;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;


public class IsNotCoastlineUDFTest {

    private static SparkSession sparkSession;
    private static final String nodesPath = "src/main/resources/romania-latest.osm.pbf.node.parquet";
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    @BeforeClass
    public static void init() {
        sparkSession = SparkSession.builder().master("local[*]").appName("Book Classification Application")
                .config("spark.sql.parquet.binaryAsString", "true").getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");
        sparkSession.udf().register("isNotCoastline", new IsNotCoastlineUDF(), DataTypes.BooleanType);
    }

    @Test
    public void selectsOnlyNonCoastlines() {
        Dataset<Row> ways = sparkSession.read().parquet(waysPath);
        Dataset<Row> coastlines = ways.select(col("id"), col("tags"), explode(col("tags")).as("tag"))
                .filter(col("tag.key").equalTo("natural").and(col("tag.value").equalTo("coastline")));

        assertTrue(coastlines.filter(callUDF("isNotCoastLine", col("tags"))).count() == 0);
    }
}