package com.telenav.internship.udf;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.not;


public class IsPlaceToHangOutUDFTest {

    private static SparkSession sparkSession;
    private static final String nodesPath = "src/main/resources/romania-latest.osm.pbf.node.parquet";
    private static final String waysPath = "src/main/resources/romania-latest.osm.pbf.way.parquet";

    @BeforeClass
    public static void init() {
        sparkSession =
                SparkSession.builder().master("local[*]").appName("Book Classification Application")
                        .config("spark.sql.parquet.binaryAsString", "true").getOrCreate();
        sparkSession.sparkContext().setLogLevel("ERROR");
        sparkSession.udf().register("isPlaceToHangOut", new IsPlaceToHangOutUDF(), DataTypes.BooleanType);
    }

    @Test
    public void identifiesCorrectlyPlacesToHangOutWhenExists(){
        Dataset<Row> nodes = sparkSession.read().parquet(nodesPath);
        Dataset<Row> places = nodes.filter(callUDF("isPlaceToHangOut", col("tags")));
        assertTrue(places.count() > 0);
    }

    @Test
    public void identifiesCorrectlyPlacesToHangOutWhenNotExists(){
        Dataset<Row> nodes = sparkSession.read().parquet(nodesPath);
        Dataset<Row> notPlaces = nodes.filter(not(callUDF("isPlaceToHangOut", col("tags"))));
        Dataset<Row> places = notPlaces.filter(callUDF("isPlaceToHangOut", col("tags")));
        assertTrue(places.count() == 0);
    }

}